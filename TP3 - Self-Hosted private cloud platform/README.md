# TP3 - Self-hosted private cloud platform

During this project I will deploy my own private cloud platform. This platform will create VM based on different hypervisors. These VM will be able to reach eachother.

I will be using *Open Nebula* as the setup tool.
I will be using KVM fro my hypervisors.
The whole project will be stocked locally.

## 1. Requirements

I will first load a default VM.
This VM will use generic/rocky9 box.

```command
$ vagrant init generic/rocky9
$ vagrant up
$ vagrant ssh
```

I will then update, install and configure my machine:

```console
$ dnf update -y
$ dnf install -vim
$ sed -i 's/SELINUX=enforcing/SELINUX=permissive/' /etc/selinux/config
$ setenforce 0 
```

Finally, I will repackage this VM in order to be able to use it:

```console
$ vagrant package --output basic-nebula.box
$ vagrant box add basic-nebula basic-nebula.box
```

I will be using Vagrant to manage my VMs.
[Here is my Vagrantfile](Vagrantfile)

- The following line will activate *Nested Virtualization*, it is necessary to have a VM inside a VM. The output of this line on VirtualBox will be seen as ```Nested VT-x/AMD-V``` on the *Acceleration* option.

```vagrantfile
v3.customize ['modifyvm', :id, '--nested-hw-virt', 'on']
```

The rest of the lines, will help create my wanted architecture.

## 2. Setup

### 2.1 Frontend

Fronted node will host the application logic to pilot KVM nodes.
It will also expose the WebUI and API.

My frontend node has as IP address: ```10.3.1.11```

#### 2.1.1 Database

I will install a MySQL server, as we are working with Rocky9, a specific version is needed:

```console 
[vagrant@frontend ~]$ wget https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm

[vagrant@frontend ~]$ sudo rpm -Uvh mysql80-community-release-el9-5.noarch.rpm 

[vagrant@frontend ~]$ dnf search mysql
# here I search for mysql-community-server.x86_64 package

[vagrant@frontend ~]$ sudo dnf -y install mysql-community-server.x86_64
```

I then start and setup the MySQL server:

```console
# start
[vagrant@frontend ~]$ sudo systemctl start mysqld

# start at boot
[vagrant@frontend ~]$ sudo systemctl enable mysqld
```

To setup the server, I must connect to MySQL as *root*. The default password for this user is located in */var/log/mysqld.log*

```console
[vagrant@frontend ~]$ sudo cat /var/log/mysqld.log
2024-04-09T08:45:42.120701Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: W8r3Sl:wSaue

[vagrant@frontend ~]$ mysql -h localhost -u root -pW8r3Sl wSaue
```
```mysql
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'Hola123#';
Query OK, 0 rows affected (0.03 sec)

mysql> CREATE USER 'oneadmin' IDENTIFIED BY 'Adios321#';
Query OK, 0 rows affected (0.03 sec)

mysql> CREATE DATABASE opennebula;
Query OK, 1 row affected (0.02 sec)

mysql> GRANT ALL PRIVILEGES ON opennebula.* TO 'oneadmin';
Query OK, 0 rows affected (0.02 sec)

mysql> SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
Query OK, 0 rows affected (0.01 sec)

mysql> exit
Bye
```

#### 2.1.2 OpenNebula

I will add this repository to my */etc/yum.repos.d/*

```
[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/6.8/RedHat/$releasever/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo2.key
gpgcheck=1
repo_gpgcheck=1
```
```console
[vagrant@frontend ~]$ sudo vim /etc/yum.repos.d/opennebula.repo

[vagrant@frontend ~]$ sudo dnf makecahe -y 

[vagrant@frontend ~]$ sudo dnf install -y opennebula opennebula-sunstone opennebula-fireedge
```

And change the following lines inside */etc/one/oned.conf*:
```
DB = [ BACKEND = "mysql",
       SERVER  = "localhost",
       PORT    = 0,
       USER    = "oneadmin",
       PASSWD  = "Adios321#",
       DB_NAME = "opennebula",
       CONNECTIONS = 25,
       COMPARE_BINARY = "no" ]
```

Finally, I will go into **oneadmin** user, that is created automatically with the installation of the opennebula modules.

```console
[vagrant@frontend ~]$ sudo su - oneadmin

[oneadmin@frontend ~]$ vim /var/lib/one/.one/one_auth
```

My one_auth looks like:

```console
[oneadmin@frontend ~]$ cat /var/lib/one/.one/one_auth 
oneadmin:hola
```

This will be the password used to connect to my WebUI.

Finally, I will start the necessary services:
```console
[vagrant@frontend ~]$ sudo systemctl start opennebula
[vagrant@frontend ~]$ sudo systemctl enable opennebula

[vagrant@frontend ~]$ sudo systemctl start opennebula-sunstone
[vagrant@frontend ~]$ sudo systemctl enable opennebula-susntone
```

#### 2.1.3 Configuration of the system

And open the necessary firewall ports:
```console
# WebUi(Sunstone)
[vagrant@frontend ~]$ sudo firewall-cmd --add-port=9869/tcp --permanent
success

#SSH
[vagrant@frontend ~]$ sudo firewall-cmd --add-port=22/tcp --permanent
success

# Daemon oned et API XML RPC
[vagrant@frontend ~]$ sudo firewall-cmd --add-port=2633/tcp --permanent
success

# Monitoring
[vagrant@frontend ~]$ sudo firewall-cmd --add-port=4124/tcp --permanent
success

[vagrant@frontend ~]$ sudo firewall-cmd --add-port=4124/udp --permanent
success

# NoVNC proxy
[vagrant@frontend ~]$ sudo firewall-cmd --add-port=29876/tcp --permanent
success

[vagrant@frontend ~]$ sudo firewall-cmd --reload
```

#### 2.1.4 Testing

I can now join my WebUI of OpenNebula at http://10.3.1.11:9869.

### 2.2 KVM One

KVM1.one will host a KVM hypervisor controlled by frontend through ssh.

#### 2.2.1 KVM

Once my node has been started, I will add the necessary opennebula repository such as with the frontend machine. And download the necesary packages.

```
[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/6.8/RedHat/$releasever/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo2.key
gpgcheck=1
repo_gpgcheck=1
```
```console
[vagrant@kvm1 ~]$ sudo vim /etc/yum.repos.d/opennebula.repo

[vagrant@kvm1 ~]$ sudo dnf makecahe -y 

[vagrant@kvm1 ~]$ sudo dnf install -y epel-release

[vagrant@kvm1 ~]$ sudo dnf install -y opennebula-node-kvm
```

Then, I start the following service:
```console
[vagrant@kvm1 ~]$ sudo dnf systemctl start libvirtd

[vagrant@kvm1 ~]$ sudo dnf systemctl enable libvirtd
```

#### 2.2.2 System 

I must open the necessary firewall ports:
```console
# VXLAN
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-port=9869/tcp --permanent
success

#SSH
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-port=22/tcp --permanent
success

[vagrant@kvm1 ~]$ sudo firewall-cmd --relaod
```

Then, I must handle SSH connections between the frontend machine and the nodes. In order to do this, I will add to the *oneadmin* user, on all machines, the **oneadmin@frontend** public key. And I will have to *key-scan* all possible hosts:

- First, note that I must add these hosts to my */etc/hosts* files on both machines:

```console
[vagrant@frontend ~]$ vim /etc/hosts

[vagrant@frontend ~]$ cat /etc/hosts
10.3.1.11 frontend frontend.one
10.3.1.21 kvm1 kvm1.one
```

I do the same for **oneadmin@kvm1** machine.
```console
[vagrant@kvm1 ~]$ vim /etc/hosts

[vagrant@kvm1 ~]$ cat /etc/hosts
10.3.1.11 frontend frontend.one
10.3.1.21 kvm1 kvm1.one
```

Then, on oneadmin@frontend I will manage the keys exchange:
```console
# copy my public key on authorized keys
[oneadmin@frontend ~]$ cp .ssh/id_rsa.pub > .ss/authorized_keys

# add the fingerprint to my machine
[oneadmin@frontend ~]$ ssh-keyscan 10.3.1.11 frontend frontend.one 10.3.1.21 kvm1 kvm1.one > .ssh/known_hosts
```

I will do the same for **oneadmin@kvm1** machine. This time, oneadmin@frontend public key, will be inserted mannually.

```console
[oneadmin@kvm1 ~]$ vim .ssh/authorized_keys

[oneadmin@kvm1 ~]$ ssh-keyscan 10.3.1.11 frontend frontend.one 10.3.1.21 kvm1 kvm1.one > .ssh/known_hosts
```

#### 2.2.3 Add node to cluster

I can now go to my OpenNebula WebUI, navigate to ```Infrastructure > Hosts``` and add my new KVM host (with 10.3.1.11 as IP address). To veryfy it works, status must be **ON**

### 2.3 Networking

I will create a VXLAN network so that VM inside of my KVM hosts will be able to communicate. VXLAN endpoint creation is managed by OpenNebula. 

- I must be able to contact VM from outside the network.
- Local VM must be able to communicate.
- VM must have Internet access.

#### 2.3.1 Virtual Network creation

This step will be done through the WebUi of OpenNebula:

```Network > Virtual Networks```
- I named my network as : *My private network*
- I changed the mode to VXLAN
- ```Conf```
    - ```eth1``` on physical device
    - ```vxlan_bridge``` on bridge 
- ```Addresses```
    - Start IP address : *10.220.220.5*
    - I wrote 10 in *Size*, to state the possible number of machines in my network
- ```Context```
    - Network address : *10.220.220.0* and Netmask : *255.255.255.0*

#### 2.3.1 Prepare network bridge

This must be done in *kvm1* node.

- **Create and configure the Linux bridge**


```console
# create 
[vagrant@kvm1 ~]$ sudo ip link add name vxlan_bridge type bridge

# start
[vagrant@kvm1 ~]$ sudo ip link set dev vxlan_bridge up 

# give address to my machine
[vagrant@kvm1 ~]$ sudo ip addr add 10.220.220.201/24 dev vxlan_bridge

# add iface to firewall public zone
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-interface=vxlan_bridge --zone=public --permanent

# NAT masquerading 
[vagrant@kvm1 ~]$ sudo firewall-cmd --add-masquerade --permanent

# reload
[vagrant@kvm1 ~]$ sudo firewall-cmd --reload
```

### 3. Using the platform

I am going to launch my first VM inside kvm1.one node.

- ```Settings > Auth```
I must add my **oneadmin@frontend** public key here, so that my machine will be able to create and connect to the VM.

- ```Storage  > Apps```
I must download to the datastore a Rocky Linux 9 image.

- ```Instances  > VMs```
Create a VM.
    - I must select Rocky Linux 9 image.
    - I must select the created Virtual Network.

- Test connection

```console
[oneadmin@kvm1 ~]$ ping 10.220.220.5
PING 10.220.220.5 (10.220.220.5) 56(84) bytes of data.
64 bytes from 10.220.220.5: icmp_seq=1 ttl=64 time=0.236 ms
64 bytes from 10.220.220.5: icmp_seq=2 ttl=64 time=1.04 ms
64 bytes from 10.220.220.5: icmp_seq=3 ttl=64 time=0.997 ms
^C
--- 10.220.220.5 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 1998ms
```

Then, I will connect myself through ssh from the frontend machine:

```console
[vagrant@frontend ~]$ sudo su - oneadmin

# start SSH agent
[oneadmin@frontend ~]$ eval $(ssh-agent)

# add private key to SSH agent
[oneadmin@frontend ~]$ ssh-add
Identity added: /var/lib/one/.ssh/id_rsa (oneadmin@frontend)

# connect to kvm1
[oneadmin@frontend ~]$ ssh -A 10.3.1.21

# connect to VM
[oneadmin@kvm1 ~]$ ssh root@10.220.220.5

[root@localhost ~]# 
```
The previous commands, are known as SSH jumps. Our private key has been added to our agent, this agent will follow the SSH connection. As ou public key has been previously added to the WebUi we are able to connect to the VM.

```console
[oneadmin@frontend ~]$ ssh -A 10.220.220.5 -J 10.3.1.21
```

- Add an internet connection

```console
[root@localhost ~]# ip route add default via 10.220.220.201
[root@localhost ~]# ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=64 time=0.236 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=64 time=1.04 ms
```

I added a default route through my kvm1 machine. This will give my VM internet

### 4. Add a new node 

The same procedure for kvm2 as for kvm1.
My machine has *10.3.1.22*

- Add the node to the WebUi ```Infrastracture >  Hosts```
- For the bridge creation I gave *10.220.220.202* as IP address.

#### 4.1/4.2/4.3/4.4 

- Note the machine must be forced to run on kvm2 ```Create VM &#8594 Deploy VM in a specific Host.
- Machine must run on the same vxlan as kvm1 machine
- Test ssh connection and ping

```console
[vagrant@kvm2 ~]$ ping 10.220.220.6
PING 10.220.220.6 (10.220.220.6) 56(84) bytes of data.
64 bytes from 10.220.220.6: icmp_seq=1 ttl=64 time=10.3 ms
64 bytes from 10.220.220.6: icmp_seq=2 ttl=64 time=1.12 ms
64 bytes from 10.220.220.6: icmp_seq=3 ttl=64 time=0.848 ms
^C
--- 10.220.220.6 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 0.848/4.087/10.295/4.390 ms
```
-------
```console 
[oneadmin@frontend ~]$ ssh -A 10.220.220.6 -J 10.3.1.22

[root@localhost ~]# ping 10.220.220.5
PING 10.220.220.5 (10.220.220.5) 56(84) bytes of data.
64 bytes from 10.220.220.5: icmp_seq=1 ttl=64 time=7.76 ms
64 bytes from 10.220.220.5: icmp_seq=2 ttl=64 time=4.99 ms
64 bytes from 10.220.220.5: icmp_seq=3 ttl=64 time=1.56 ms
^C
--- 10.220.220.5 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2016ms
rtt min/avg/max/mdev = 1.557/4.766/7.756/2.535 ms
```

Finally I will be able to capture the exchange between my machines.

```console
[vagrant@kvm2 ~]$ sudo tcpdump -i eth1 -w kvm2_eth1-pcap
```
- Here you can find the [eth1 capture](kvm2_eth1.pcap)

```console
[vagrant@kvm2 ~]$ sudo tcpdump -i vxlan_brigde -w kvm2_vxlan-bridge-pcap
```
- Here you can find the [vxlan-bridge capture](kvm2_vxlan-bridge.pcap)
