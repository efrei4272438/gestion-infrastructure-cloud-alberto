 # TP2 - Network boot

 This part of the project will be using PXE protocol. 
 PXE is a protocol used to launch a VM without hard disk through the network.
 

To do this I will first launch a VM

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.hostname = "pxe.tp2.efrei"
  config.vm.network "private_network", ip: "10.1.1.2"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "pxe.tp2.efrei"
  end
end
```


And I will install some services like:

## DHCP server installation

DHCP protocol will be used to attribute an IP address to the machines that will connect to the network

```console
$ sudo dnf -y install dhcp-server
```

Which configuration file (etc/dhcp/dhcpd.conf) will look like:

```
default-lease-time 600;
max-lease-time 7200;
authoritative;

# PXE specifics
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet <NETWORK_ADDRESS> netmask 255.255.255.0 {
    range dynamic-bootp 10.1.1.5 10.1.1.10;
    
    # add follows
    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 10.1.1.2;

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}
```

Then I start my dhcp server and open the necessary firewall port:

```console
$ sudo systemctl enable dhcpd
$ sudo systemctl start dhcpd

$ sudo firewall-cmd --add-service=dhcp --permanent
$ sudo firewall-cmd --reload
```

## TFTP server installation

TFTP protocol will be used to download data from the server.

```console
$ sudo dnf -y install tftp-server
$ sudo systemctl enable --now tftp.socket
$ sudo firewall-comd --add-service=tftp --permanent
$ sudo firewall-cmd --reload
```

Then, I will get my ISO image from my computer using scp command: 

```console
$ scp t1110@10.1.1.1:/home/t1110/Downloads/Rocky-9.3-x86_64-minimal.iso /home/vagrant/
```

And I will follow this commands:

```console
# prepare installation
$ sudo dnf -y install syslinux

# move the file pxelinux.0 to the folder that will be used by HTTP/TFTP server
$ sudo cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/

# prepare the environment
$ mkdir -p /var/pxe/rocky9
$ sudo mkdir /var/lib/tftpboot/rocky9

# adapt the path to the rocky iso on my VM
$ sudo systemctl daemon-relaod
$ sudo mount -t iso9660 -o loop,ro /home/vagrant/Rocky-9.3-x86_64-minimal.iso /var/pxe/rocky9

# take from rocky iso what's necessary
$ sudo cp /var/pxe/rocky9/images/pxeboot/{vmlinuz,initrd.img} /var/lib/tftpboot/rocky9/
$ sudo  cp /usr/share/syslinux/{menu.c32,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/

# prepare boot options file
$ sudo mkdir /var/lib/tftpboot/pxelinux.cfg
```

And I will modify my */var/lib/tftpboot/pxelinux.cfg/default*

```
default vesamenu.c32
prompt 1
timeout 60

display boot.msg

label linux
  menu label ^un manguito clasico tu sabe
  menu default
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img ip=dhcp inst.repo=http://10.0.1.2/rocky9
label rescue
  menu label ^Rescue installed system
  kernel rocky9/vmlinuz
  append initrd=rocky9/initrd.img rescue
label local
  menu label Boot from ^local drive
  localboot 0xffff
```

## Apache server installation

```console
$ sudo dnf -y install httpd
```

I created */etc/httpd/conf.d/pxeboot.conf*:

```
Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    # access permission
    Require ip 127.0.0.1 10.1.1.0/24
</Directory>
```

Started apache and opened the necessary firewall ports:

```console
$ sudo firewall-cmd --add-port=80/tcp --permanent
$ sudo firewall-cmd --reload
```

## Test

Once all the previous steps have been done, I can mannualy create a VM that will take the Rocky iso.

This machine should have the following configuration:
- Enough memory to do the installation
- Network boot activated
- Same host-only network interface as the server
- Promiscuous mode changed

I have joined [here](pxeboot.pcap) a tcpdump capture during the boot of a new VM.