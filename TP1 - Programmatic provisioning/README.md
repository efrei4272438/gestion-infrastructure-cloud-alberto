 
# TP1 Programmatic provisioning

This project's objective is to automatize the creation of virtual boxes.
I will be using Virtualbox as hypervisor and Vagrant in order to pilot the hypervisor.

## 1. A first VM

### ez startut

In order to be able to use vargant I will create a repertory for each new virtual machine. 
I will be using Rocky 9 as OS.

```console
$ mkdir vagrant0
$ cd vagrant0/

$cat Vagrantfile
```

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"

end
```
This is what a Vagrantfile looks like. With this file I am able to launch a simple VM without any configuration.

In order to do so:
```console
$ vagrant up
```

In order to connect:
```console
$ vagrant ssh
```

In order to destroy the machine:
```console
$ vagrant halt
$ vagrant destroy -f
```

### A little of configuration

This new Vagrantfile will:
- Attribute an IP address
- Attribute a hostname
- Change vagrant name
- Add 2GB of RAM
- Have a disk size of 20G

```

Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.network "private_network", ip: "10.1.1.11"
  config.vm.disk :disk, size: "20GB", primary: true
  config.vm.provider "virtualbox" do |vb|
    vb.name = "ezconf.tp1.efrei"
    vb.memory = 2048
  end
end

```

Here the results:

```console
[vagrant@ezconf ~]$ ip a
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:03:97:6a brd ff:ff:ff:ff:ff:ff
    altname enp0s8
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe03:976a/64 scope link 
       valid_lft forever preferred_lft forever

[vagrant@ezconf ~]$ cat /proc/meminfo
MemTotal:        2002652 kB
```

By changing the default configuration of my Vagrantfile I can create personalized VMs.

## 2. Initialization script

The following Vagrantfile makes reference to a VM that has been given a bash script to execute at start.

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.provision "shell", path: "script.sh"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "ezconf.tp1.efrei"
    vb.memory = 2048
  end
end
```

My script.sh looks like this:

```console
#!/bin/bash

sudo dnf install -y vim
sudo dnf install -y python3
sudo dnf update -y
```

This script will install vim, python3 and then update the machine:

```console
$ vagrant up
$ vagrant ssh

[vagrant@ezconf ~]$ cat hola
hola

[vagrant@ezconf ~]$ python3
Python 3.9.18 (main, Jan  4 2024, 00:00:00) 
[GCC 11.4.1 20230605 (Red Hat 11.4.1-2)] on linux
Type "help", "copyright", "credits" or "license" for more information.
```

## 3. Repackaging

Repackaging means to create a customized image from a created VM. This means we can store a desired configuration and create all the VM that we want. 

```console
$ vagrant package --output custom-efrei.box

$ vagrant box add custom-efre custom-efrei.box

$ vagrant box list
custom-efrei   (virtualbox, 0)
```

And to repackage this machine in a new folder:

```console
$ vagrant init custom-efrei
```

## 4. Multiple VM

We any machines we want with a single Vagrantfile. Here I launch two machines on the same network, different hostnames and RAM memory:

```
Vagrant.configure("2") do |config|
  config.vm.define "machine1" do |m1|
    m1.vm.box = "generic/rocky9"
    m1.vm.hostname = "machine1.tp1.efrei"

    m1.vm.network :private_network, ip: "10.1.1.101"

    m1.vm.provider :virtualbox do |v1|
      v1.memory = 2048
      v1.customize ["modifyvm", :id, "--name", "machine.tp1.efrei"]
    end
  end
  config.vm.define "machine2" do |m2|
    m2.vm.box = "generic/rocky9"
    m2.vm.hostname = 'machine2.tp2.efrei'

    m2.vm.network :private_network, ip: "10.1.1.102"

    m2.vm.provider :virtualbox do |v2|
      v2.memory = 1024
      v2.name = "machine2.tp1.efrei"
    end
  end
end
```

Once my machines have been started, I am going to verify that these mahcines are on the same network.

```console
$ vagrant ssh machine2

[vagrant@machine2 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=0.238 ms
```

## 5. Cloud-init

Cloud-init is used to do an automatic configuration of a VM from booting. I will launch a VM and install *cloud-init* service:

```console
[vagrant@machine2 ~]$ sudo dnf -y install cloud-init
[vagrant@machine2 ~]$ sudo systemctl enable cloud-init
```

Then the machine will be repackaged :

```console
$ vagrant box list
cloud-efrei    (virtualbox, 0)
```

In order to create a new VM with a specific *cloud-init* configuration there are two methods:
- Generating mannually the iso file:

```console
genisoimage -output cloud-init.iso -volid cidata -joliet -r meta-data user-data
```

This command makes reference to files meta-data:

```cloud-init
#cloud-init
local-hostname: cloud-init.tp1.efrei
```

And to user-data:
```cloud-init
#cloud-config
users:
  - name: tillo
    primary_group: tillo
    groups: wheel 
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$ajra8rB1mQPgIqhh$i4ID0xwnIMohx0n8bHOy4fs3MVJBU.TTO9aRzmJKbeZWtDwly930LJEz5X7X8prMoZj.XfrcEWVhe1ZEcM68d1
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1l3R4CNTE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA 

```

Finally my Vagrantfile will look like this:

```
Vagrant.configure("2") do |config|
  config.vm.box = "cloud-efrei"
  config.vm.disk :dvd, name: "installer", file: "./cloud-init.iso"
  end
end
```

- Give vagrant a cloud-init.yml file

My user data file looks like:

```cloud-init
#cloud-config
users:
  - name: tillo
    gecos: Super adminsys
    primary_group: tillo
    groups: wheel
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: $6$oal3hOBfC4ucE4hW$JcY1XMPBrpLS8SC1ytVfOvxHCJHh8eKBUuOO9DhBfx2UZ03rCK3Kt52XXtFODTAOP9k1kjf11cCvX5lcXMNnh0
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3Nz34RFRU51NTE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA
```

And my Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "test"
  config.vm.cloud_init :user_data, content_type: "text/cloud-config", path: "user_data.yml"
end
```

To verify that this works, I entered my machine and looked for tillo username at */etc/passwd* file:

```console
$ vagrant ssh

[vagrant@rocky9 ~]$ cat /etc/passwd
tillo:x:1001:1001:Super adminsys:/home/tillo:/bin/bash
```

